#!/usr/bin/env bash
# This script will create a sshfs to the Mines Isengard server
# It also assumes that sshkeys are setup

if [ -z "$1" ]; then
    MULTIPASS_USER="warfield"
else
    MULTIPASS_USER=$1
fi

# What folder to mount to locally
MOUNT_LOC="$HOME/isengard"

# What folder to mount to on Isengard, NO RELETIVE PATHS DUE TO LOCAL HOST TUNNEL
REMOTE_LOC="/u/au/cx/warfield"

# Remote tunneling server
BRIDGE="$MULTIPASS_USER@imagine.mines.edu"

# Local port to bind the connection to
PORT=2229

# Close down any existing connections before trying to establish a new one
fusermount -u $MOUNT_LOC > /dev/null
ps ax | grep "ssh -f $BRIDGE -L $PORT:isengard:22 -N" \
        | awk '{print $1}' | \
	xargs -i kill {} 2>/dev/null

# Try direct connection, only works if on the Mines network
if timeout 2 \
    sshfs $MULTIPASS_USER@isengard.mines.edu:$REMOTE_LOC $MOUNT_LOC \
    > /dev/null;
then
    echo "Success! Direct connection to Isengard made!"

# Else tunnel through Imagine
else
    echo "Connection not made, attempting proxy through imagine"
    # Bind $PORT to forward to one jumpbox pointing to port 22 on isengard
    ssh -f $BRIDGE -L $PORT:isengard:22 -N

    # Mount a sshfs through localhost on $PORT
   
    ssh-copy-id -p $PORT $MULTIPASS_USER@localhost:$REMOTE_LOC

    sshfs -p $PORT $MULTIPASS_USER@localhost:$REMOTE_LOC $MOUNT_LOC \
        && echo "Success! Indirect Connection made!" \
        || echo "Failure? Are you behind the Great Fire Wall?"
fi
